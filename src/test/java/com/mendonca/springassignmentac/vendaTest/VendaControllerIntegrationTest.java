package com.mendonca.springassignmentac.vendaTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mendonca.springassignmentac.entity.Venda;
import com.mendonca.springassignmentac.repository.VendaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class VendaControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private VendaRepository vendaRepository;

    @Test
    public void testCreateVenda() throws Exception {
        String nomeCliente = "João";
        Venda newVenda = new Venda(800, nomeCliente, "Fabiola");

        String url = "/addVenda";

        MvcResult mvcResult = mockMvc.perform(post(url)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(newVenda)))
                .andExpect(status().isOk()).andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Integer vendaId = Integer.valueOf(response);

        Venda savedVenda = vendaRepository.findById(vendaId).get();

        assertThat(savedVenda.getCliente()).isEqualTo(nomeCliente);
    }

    @Test
    public void testUpdateVenda() throws Exception {
        String nomeCliente = "Paola";
        Venda newVenda = new Venda(1, 800, nomeCliente, "Fabiola");

        String url = "/update";

        MvcResult mvcResult = mockMvc.perform(put(url)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(newVenda)))
                .andExpect(status().isOk()).andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Integer vendaId = Integer.valueOf(response);

        Venda savedVenda = vendaRepository.findById(vendaId).get();

        assertThat(savedVenda.getCliente()).isEqualTo(nomeCliente);
    }

    @Test
    public void testDeleteVenda() throws Exception {
        Integer vendaId = 1;
        String url = "/delete/" + vendaId;

        mockMvc.perform(delete(url)).andExpect(status().isOk());

        Optional<Venda> result = vendaRepository.findById(vendaId);

        assertThat(result).isNotPresent();
    }

    @Test void testListVendas() throws Exception {
        String url = "/vendas";

        MvcResult mvcResult = mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andReturn();

        String JsonResponse = mvcResult.getResponse().getContentAsString();
        Venda[] vendas = objectMapper.readValue(JsonResponse, Venda[].class);

        assertThat(vendas).hasSize(2);

    }

}
